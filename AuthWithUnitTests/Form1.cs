﻿using System;
using System.Windows.Forms;

namespace AuthWithUnitTests
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "")
            {
                MessageBox.Show("Вы забыли ввести логин", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if(textBox2.Text == "")
            {
                MessageBox.Show("Вы забыли ввести пароль", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Auth auth = new Auth();

            if(auth.GetAuth(textBox1.Text,textBox2.Text) == "good")
            {
                MessageBox.Show("Аккаунт валиден", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            else if(auth.GetAuth(textBox1.Text, textBox2.Text) == "bad")
            {
                MessageBox.Show("Неверный логин или пароль", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if(auth.GetAuth(textBox1.Text, textBox2.Text) == "error")
            {
                MessageBox.Show("Произошла ошибка, проверьте ваше интернет-соединение", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
