﻿using xNet;

namespace AuthWithUnitTests
{
   public class Auth
    {
        public string GetAuth(string name, string pass)
        {
            try
            {
                using (HttpRequest req = new HttpRequest())
                {
                    req.UserAgent = Http.ChromeUserAgent();
                    req.Cookies = new CookieDictionary();
                    req.KeepAlive = true;


                    req.Referer = "https://www.yandex.ru/";
                    var Params = new RequestParams();
                    
                    Params["login"] = name;
                    Params["passwd"] = pass;
                    

                    string s = req.Post("https://passport.yandex.ru/passport?mode=auth&retpath=https://mail.yandex.ru", Params).ToString();

                    if (s.Contains("logout"))
                    {

                        return "good";
                    }

                    else if (s.Contains("control__checkbox-icon"))
                    {
                        return "bad";
                    }
                    else
                    {
                        return "error";
                    }
                }
            }
            catch
            {
                return "error";
            }
            
        }
    }
}
