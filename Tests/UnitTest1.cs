﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AuthWithUnitTests;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        Auth auth = new Auth();

        [TestMethod]
        public void GoodTest()
        {
            //usermenu__user-name
            Assert.AreEqual(auth.GetAuth("BugReeport@yandex.ru", "Az123456"), "good");
        }

        [TestMethod]
        public void BadTest()
        {
            Assert.AreEqual(auth.GetAuth("BugReeport@yandex.ru", "At4546565"), "bad");
        }
        
    }
}
